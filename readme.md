# Modify Git metadata

This project is useful if you want to extensively alter commit
metadata in a Git repository. This script allows you to change
message, author, committer and date of chosen commits.

Use on your own risk.

## Installation

Install [git-filter-repo](https://github.com/newren/git-filter-repo).
Depending on you python version, it might be necessary to install package tomli:

```sh
pip3 install tomli
```
Download [modify_git_metadata.py](modify_git_metadata.py) to your working directory.

## Usage

### 1. Create a copy of your repo
For example something like this:
```sh
git clone --no-local source-repo target-repo
```

### 2. Extract metadata from source repo
  
```sh
git filter-repo \
  --source source-repo \
  --target target-repo \
  --commit-callback '
      import sys, os
      sys.path.insert(0, os.getcwd())
      import modify_git_metadata
      modify_git_metadata.printCommit(commit)
  ' > mapping.toml
```

### 3. Modify metadata

Manually fix mapping.toml - at the end of the file, there are extra lines.
Please delete them. They look like this:
```
Parsed 151 commits HEAD is now at 10a1b96 test

New history written in 0.04 seconds; now repacking/cleaning...
Repacking your repo and cleaning out old unneeded objects
Completely finished after 0.09 seconds.
```

Then you can modify commit metadata as you like.

### 4. Apply changes to target repo

```sh
git filter-repo \
    --source source-repo \
    --target target-repo \
    --commit-callback '
        import sys, os
        sys.path.insert(0, os.getcwd())
        import modify_git_metadata
        modify_git_metadata.modifyCommit(commit)
    '
```

This will modify the following metadata for all commits:

 - message
 - author date
 - author name
 - author email
 - committer date
 - committer name
 - committer email

The script makes the assumption that you always want committer and author
information to be the same. If you want them to modify separately, please
modify [modify_git_metadata.py](modify_git_metadata.py) functions `printCommit`
and `modifyCommit`.


To see how commit IDs changed, you can run the following in the target repo:

```sh
git show-ref | awk '/refs\/replace\// { print substr($2,14) " ==> " $1 }'
```
