#!/usr/bin/python3

import subprocess
import tomli
from datetime import datetime as datetime

mapping = None

def parseDate(git_date):
    timeComponents = git_date.decode().split(" ")
    timestamp = int(timeComponents[0])
    timezoneOffset = timeComponents[1]
    date = datetime.utcfromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")
    return [date, timezoneOffset]


def dateToGit(isoDate, timeZone):
  timestamp = datetime.fromisoformat(isoDate).timestamp()
  dateString = str(int(timestamp)) + " " + timeZone
  return dateString.encode()


def printCommit(commit):
  print("[" + commit.original_id.decode() + "]")
  print("message = \"\"\"" + commit.message.decode() + "\"\"\"")

  author_date = parseDate(commit.author_date)
  print("author_date_utc = \"" + author_date[0] + "\"")
  print("author_date_tz = \"" + author_date[1] + "\"")
  print("author_name = \"" + commit.author_name.decode() + "\"")
  print("author_email = \"" + commit.author_email.decode() + "\"")

  # committer_date = parseDate(commit.committer_date)
  # print("committer_date_utc = \"" + committer_date[0] + "\"")
  # print("committer_date_tz = \"" + committer_date[1] + "\"")
  # print("committer_name = \"" + commit.committer_name.decode() + "\"")
  # print("committer_email = \"" + commit.committer_email.decode() + "\"")

  print("")


def loadMapping():
  with open("mapping.toml", "rb") as f:
    print('loading mapping')
    return tomli.load(f)


def modifyCommit(commit):
  global mapping
  if mapping == None:
    mapping = loadMapping()

  original_id = commit.original_id.decode()
  mappedCommit = mapping[original_id]
  if mappedCommit != None:
    commit.message = mappedCommit['message'].encode()

    commit.author_date = dateToGit(mappedCommit['author_date_utc'], mappedCommit['author_date_tz'])
    commit.author_name = mappedCommit['author_name'].encode()
    commit.author_email = mappedCommit['author_email'].encode()

    # commit.committer_date = dateToGit(mappedCommit['committer_date_utc'], mappedCommit['committer_date_tz'])
    # commit.committer_name = mappedCommit['committer_name'].encode()
    # commit.committer_email = mappedCommit['committer_email'].encode()

    commit.committer_date = dateToGit(mappedCommit['author_date_utc'], mappedCommit['author_date_tz'])
    commit.committer_name = mappedCommit['author_name'].encode()
    commit.committer_email = mappedCommit['author_email'].encode()
